Tic Tac Toe Game Plan
Overview:
RULES FOR TIC-TAC-TOE

1. The game is played on a grid that's 3 squares by 3 squares.

2. You are X, your friend (or the computer in this case) is O. Players take turns putting their marks in empty squares.

3. The first player to get 3 of her marks in a row (up, down, across, or diagonally) is the winner.

4. When all 9 squares are full, the game is over. If no player has 3 marks in a row, the game ends in a tie.
					
 HOW CAN I WIN AT TIC-TAC-TOE?
* To beat the computer (or at least tie), you need to make use of a little bit of strategy. Strategy means figuring out what you need to do to win.

* Part of your strategy is trying to figure out how to get three Xs in a row. The other part is trying to figure out how to stop the computer from getting three Os in a row.

* After you put an X in a square, you start looking ahead. Where's the best place for your next X? You look at the empty squares and decide which ones are good choices�which ones might let you make three Xs in a row.

* You also have to watch where the computer puts its O. That could change what you do next. If the computer gets two Os in a row, you have to put your next X in the last empty square in that row, or the computer will win. You are forced to play in a particular square or lose the game.

* If you always pay attention and look ahead, you'll never lose a game of Tic-Tac-Toe. You may not win, but at least you'll tie.











1. TTT: Player Clicks
* Declare a variable to represent  x and initialize it to a value of �x� and o
* Declare a array to represent the squares of a tic tac toe board and initialize it.
* Add an event listener to the window object for the DOMContentLoaded event.
* In this event listener � add a click event handler to the div element that represent the tic tac toe grid
* In the click event handler check the event target�. Ignore the event if it�s id does not begin with the id of one of the nine �square-�(0-9) <div> tags.
* If the event target�s id does begin with �square-� then 


2. TTT: Game Status


3. TTT: New Game


4. TTT: Giving Up


5. TTT: Saving Game State


6. TTT: Nightmare Mode: You vs. Machine

